var selfactivity = selfactivity || {};

/*****************************************************************************
 * Persistence: storage and retrieval of persistent data
 */
selfactivity.Persistence = function() {
};

selfactivity.Persistence.prototype.data = {};

selfactivity.Persistence.prototype.getPrefix = function() {
    var id = (chrome && chrome.runtime && chrome.runtime.id)?chrome.runtime.id:'test';
    return id;
};

selfactivity.Persistence.prototype.init = function() {
    this.loadPersistentData();
};

selfactivity.Persistence.prototype.loadPersistentData = function() {
    var stringifiedData = localStorage[this.getPrefix()];
    var persistentData = null;
    try {
        persistentData = JSON.parse(stringifiedData);
    } catch (excpetion) {
        //ignore parsing exceptions
    }
    if (!persistentData) {
        persistentData = {};
    }
    this.data = persistentData;
    return this;
};

selfactivity.Persistence.prototype.storePersistentData = function(persistentData) {
    for (var key in persistentData) {
        this.data[key] = persistentData[key];
    }
    this.storeValues();
}

selfactivity.Persistence.prototype.storeValues = function() {
    var stringifiedData = JSON.stringify(this.data);
    localStorage[this.getPrefix()] = stringifiedData;
    return this;
};

selfactivity.Persistence.prototype.setValue = function(name, value) {
    this.data[name] = value;
    this.storeValues();
};

selfactivity.Persistence.prototype.getValue = function(name) {
    return this.data[name];
};   

selfactivity.Persistence.prototype.getValues = function() {
    return this.data;
};   

/*
 * Persistence: *END* 
 *****************************************************************************/

/*****************************************************************************
 * AsyncBackground: asynchronous communication with the background script
 */
selfactivity.AsyncBackground = function() {
};

selfactivity.AsyncBackground.prototype.getSession = function(callback) {
    this._messageBackground('getSession', null, callback);  
};

selfactivity.AsyncBackground.prototype.setSession = function(session, callback) {
    this._messageBackground('setSession', session, callback)
};

selfactivity.AsyncBackground.prototype.reloadPersistentData = function(callback) {
    this._messageBackground('reloadPersistentData', null, callback);  
};

selfactivity.AsyncBackground.prototype.openReport = function(callback) {
    this._messageBackground('openReport', null, callback);  
};

selfactivity.AsyncBackground.prototype._messageBackground = function(method, params, callback) {
    chrome.runtime.sendMessage({'method':method, 'params':params}, 
        function(response) {
            if (callback) {
                callback(response);
            }
        });
};

selfactivity.AsyncBackground.prototype.init = function() {
};

/*
 * AsyncBackground: *END* 
 *****************************************************************************/

/*****************************************************************************
 * Session: session storage object. 
 * Conmtainer for data that is persistent through session.
 * Stored in the background page.
 * It's a responsibility of a content script to request the session
 * at the beginning of it's actions and send updated session back before 
 * it leaves the page on which it works.
 */
selfactivity.Session = function() {
};

selfactivity.Session.PERSISTENT_KEYS = ['username', 'password', 
'maxJobApplications', 'excludedPhraseList','actionDelay', 'searchTitle', 
'searchKeywords', 'searchWhere', 'searchDistance', 'searchPosted', 
'searchType', 'loginFail' ];

selfactivity.Session.prototype.init =  function() {
};

selfactivity.Session.prototype.isEnabled = function() {
    return this.enabled;
};

selfactivity.Session.prototype.setEnabled = function(val) {
    this.enabled = val;
    return this;
};

selfactivity.Session.prototype.getJobs = function() {
    if (null==this.jobs) {
        this.jobs = {};
    }
    return this.jobs;
};

selfactivity.Session.prototype.setJobs = function(val) {
    this.jobs = val;
    return this;
};

selfactivity.Session.prototype.getJobData = function(id) {
    var jobs = this.getJobs();
    var data = jobs[id];
    if (data==null) {
        data = {};
    }
    return data;
};

selfactivity.Session.prototype.setJobData = function(id, data) {
    var currentData = this.getJobData(id);
    for (var key in data) {
        currentData[key] = data[key];
    }
    this.jobs[id]=currentData;
};

selfactivity.Session.prototype.getJobApplicationCount = function() {
    var jobsData = this.getJobs();
    var count = 0;
    for (var key in jobsData) {
        var jobData = jobsData[key];
        if ('applied'==jobData.status) {
            count++;
        }
    }
    return count;
}

selfactivity.Session.prototype.getCurrentJobId = function() {
    return this.currentJobId;
};

selfactivity.Session.prototype.setCurrentJobId = function(val) {
    this.currentJobId = val;
    return this;
};

selfactivity.Session.prototype.getUsername = function() {
    return this.username;
};

selfactivity.Session.prototype.setUsername = function(val) {
    this.username = val;
    return this;
};

selfactivity.Session.prototype.getPassword = function() {
    return this.password;
};

selfactivity.Session.prototype.setPassword = function(val) {
    this.password = val;
    return this;
};

selfactivity.Session.prototype.getLoginFail = function() {
    return this.loginFail;
};

selfactivity.Session.prototype.setLoginFail = function(val) {
    this.loginFail = val;
    return this;
};

selfactivity.Session.prototype.getMaxJobApplications = function() {
    return this.maxJobApplications;
};

selfactivity.Session.prototype.setMaxJobApplications = function(val) {
    this.maxJobApplications = val;
    return this;
};

selfactivity.Session.prototype.getExcludedPhraseList = function() {
    return this.excludedPhraseList;
};

selfactivity.Session.prototype.setExcludedPhraseList = function(val) {
    this.excludedPhraseList = val;
    return this;
};

selfactivity.Session.prototype.getExcludedPhrases = function() {
    var phraseList = this.getExcludedPhraseList();
    if (phraseList) {
        return phraseList.split(',');

    } else {
        return [];
    }
};

selfactivity.Session.prototype.getActionDelay = function() {
    return this.actionDelay;
};

selfactivity.Session.prototype.setActionDelay = function(val) {
    this.actionDelay = val;
    return this;
};

selfactivity.Session.prototype.getSearchTitle = function() {
    return this.searchTitle;
};

selfactivity.Session.prototype.setSearchTitle = function(val) {
    this.searchTitle = val;
    return this;
};

selfactivity.Session.prototype.getSearchKeywords = function() {
    return this.searchKeywords;
};

selfactivity.Session.prototype.setSearchKeywords = function(val) {
    this.searchKeywords = val;
    return this;
};

selfactivity.Session.prototype.getSearchWhere = function() {
    return this.searchWhere;
};

selfactivity.Session.prototype.setSearchWhere = function(val) {
    this.searchWhere = val;
    return this;
};

selfactivity.Session.prototype.getSearchDistance = function() {
    return this.searchDistance;
};

selfactivity.Session.prototype.setSearchDistance = function(val) {
    this.searchDistance = val;
    return this;
};

selfactivity.Session.prototype.getSearchPosted = function() {
    return this.searchPosted;
};

selfactivity.Session.prototype.setSearchPosted = function(val) {
    this.searchPosted = val;
    return this;
};

selfactivity.Session.prototype.getSearchParams = function() {
    return {
        'Title': this.searchTitle,
        'Keywords': this.searchKeywords,
        'Where': this.searchWhere,
        'Distance': this.searchDistance,
        'Posted': this.searchPosted,
        'Type': this.searchType
    };
}

selfactivity.Session.prototype.getSessionTabId = function() {
    return this.sessionTabId;
};

selfactivity.Session.prototype.setSessionTabId = function(val) {
    this.sessionTabId = val;
    return this;
};

selfactivity.Session.prototype.getCurrentTabId = function() {
    return this.currentTabId;
};

selfactivity.Session.prototype.setCurrentTabId = function(val) {
    this.currentTabId = val;
    return this;
};

selfactivity.Session.prototype.getCurrentSearchResultsPage = function() {
    return this.currentSearchResultsPage;
};

selfactivity.Session.prototype.setCurrentSearchResultsPage = function(val) {
    this.currentSearchResultsPage = val;
    return this;
};

selfactivity.Session.prototype.getRuntimeId = function() {
    return this.runtimeId;
};

selfactivity.Session.prototype.setRuntimeId = function(val) {
    this.runtimeId = val;
    return this;
};

selfactivity.Session.prototype.loadData = function(sessionData) {
    if (sessionData) {
        for (var key in sessionData) {
            this[key] = sessionData[key];
        }
    }
};

selfactivity.Session.prototype.loadPersistentData = function(sessionData) {
    if (sessionData) {
        for (var key in sessionData) {
            if ($.inArray(key, selfactivity.Session.PERSISTENT_KEYS)>=0) {
                this[key] = sessionData[key];
            }
        }
    }
};

selfactivity.Session.prototype.getPersistentData = function(sessionData) {
    var persistentData = {};
    for (var i in selfactivity.Session.PERSISTENT_KEYS) {
        var key = selfactivity.Session.PERSISTENT_KEYS[i];
        var value = this[key];
        persistentData[key] = value;
    }
    return persistentData;
};

selfactivity.Session.prototype.allChecksBeforeStart = function() {
    var checkFunctions = ["checkAccountDetailsSet", "checkAccountDetailsWork", "checkSearchSettingsSet"];
    var results = [];
    for (var i=0; i<checkFunctions.length; i++) {
        var functionName = checkFunctions[i];
        var result = this[functionName]();
        if (null!=result) {
            results.push(result);
        }
    }
    if (results.length>0) {
        return results;
    } 
    return null;
}

selfactivity.Session.prototype.checkAccountDetailsSet = function() {
    if (this.username && this.username.length && this.password && this.password.length) {
        return null;
    }
    return "You need to set Universal Jobmatch account details.";
};

selfactivity.Session.prototype.checkAccountDetailsWork = function() {
    if (this.loginFail) {
        return "Your Universal Jobmatch account details didn't work.";
    }
    return null;
};

selfactivity.Session.prototype.checkSearchSettingsSet = function() {
    if (this.searchTitle && this.searchTitle.length && this.searchWhere && this.searchWhere.length) {
        return null;
    }
    return "You should set at least Title and Where in job search settings.";

};

/*
 * Session: *END* 
 *****************************************************************************/

/*****************************************************************************
 * SessionProxy: session proxy for content scripts.
 * When any data is sent between scripts via messages it's deserialized to plain objects.
 * On receiving that we convert it back to our classes.
 * Session object assumes being in background script and may use some apis not 
 * available in content scripts, so it's better to have a seprate class for that.
 */
selfactivity.SessionProxy = function() {
};

selfactivity.SessionProxy.prototype = new selfactivity.Session();

selfactivity.SessionProxy.factory = function(sessionData) {
    var proxy = new selfactivity.SessionProxy();
    proxy.loadData(sessionData);
    return proxy;
};

/*
 * SessionProxy: *END* 
 *****************************************************************************/

/*****************************************************************************
 * ContentScript: base class for content scripts
 */
selfactivity.ContentScript = function() {
};

selfactivity.ContentScript.MAIN_SEARCH_SELECTOR = "#mainPws";
selfactivity.ContentScript.MAIN_PARAMS_SELECTOR = "#currentSelections";

selfactivity.ContentScript.prototype.updateBackgroundSession = function() {
    this.asyncBackground.setSession(this.session);
};

/**
 * Return time that will lapse before the action on page is performed.
 */
selfactivity.ContentScript.prototype.getActionDelay = function() {
    var averageDelay = this.session.getActionDelay();
    if (!averageDelay) {
        averageDelay = 5;
    }
    var currentDelay = Math.round( averageDelay * 1000 * (0.5 + Math.random()));
    return currentDelay;
}

/**
 * Perform a final action on the page. 
 * Do all bookkeping with the session state etc and then perform te final action
 * passed in callback. 
 * The action will be delayed for a random amount of time contrlled
 * by the average delay set in extension options.
 * While the script is waiting the delay, there will be a modal dialog shown
 * with a message (if passed).
 * There is an assumption that the final action will end in a click or somethng
 * similar which will cause the site to navigate to different page.
 */
selfactivity.ContentScript.prototype.doFinalActionOnPage = function(callback, message) {
    var self = this;
    this.updateBackgroundSession();
    this.showOverlay();
    this.showDialog(message, callback==null);
    if (callback) {
        var delay = this.getActionDelay();
        self.finalActionTimeoutId = setTimeout(function() {
            if (self.session.isEnabled()) {
                callback();
            }
        }, delay);
    }
};

selfactivity.ContentScript.prototype.mainSearchFinalAction = function() {
    var self = this;
    var searched = false;
    var searchSelector = selfactivity.ContentScript.MAIN_SEARCH_SELECTOR;
    $(searchSelector+' input[type="text"]').each(function() {
        var idParts = this.id.split('__');
        var humanName = idParts[idParts.length-1].substr(2);
        var methodName = "getSearch" + humanName;
        var fieldValue = self.session[methodName]();
        $(this).val(fieldValue);
    });
    $(searchSelector+' input[type="submit"]').first().each( function() {
        searched = true;
        $(this).click();
    });
}

selfactivity.ContentScript.prototype.doMainSearch = function() {
    var self = this;
    this.doFinalActionOnPage(function(){
        self.mainSearchFinalAction();
    }, "Searching for jobs");
};

/**
-> store job application status for the job id given
-> clear job id in the session
-> repeat main search after
*/
selfactivity.ContentScript.prototype.doAfterJobApplication = function(jobId, jobStatus) {
    var currentJobId = (jobId!=null)?jobId:this.session.getCurrentJobId();
    this.session.setJobData(currentJobId, {status:jobStatus});
    this.session.setCurrentJobId(null);
    this.doMainSearch();
};

selfactivity.ContentScript.prototype.initIfCurrent = function(callback) {
    console.log('this needs to be overriden in subclass!');
    throw 'this needs to be overriden in subclass!';
}

selfactivity.ContentScript.prototype.doIfCurrent = function(callback) {
    if (this.session.getCurrentTabId()==this.session.getSessionTabId()) {
        callback();
    }
}

selfactivity.ContentScript.prototype.initIfEnabled = function(callback) {
    var self = this;
    this.doIfCurrent(function() {
        self.initIfCurrent();
    });
};

/*
only call the callback if the enabled flag is set and the current tab is the same
as the one which the plugin was called in initially.
*/
selfactivity.ContentScript.prototype.doIfEnabled = function(callback) {
    var self = this;
    var enabled = self.session.isEnabled();
    if (enabled) {
        callback();
    }
};

selfactivity.ContentScript.prototype.initWithSession = function() {
    var self = this;
    self.asyncBackground = new selfactivity.AsyncBackground();
    this.doIfEnabled(function() {
        self.initIfEnabled();
    });
};

selfactivity.ContentScript.prototype.doWithSession = function(callback) {
    var self = this;
    self.asyncBackground.getSession(function(sessionData){
        self.session = selfactivity.SessionProxy.factory(sessionData);
        if (callback) {
            callback();
        }
    });

};

selfactivity.ContentScript.prototype.init = function() {
    var self = this;
    self.asyncBackground = new selfactivity.AsyncBackground();
    this.doWithSession(function() {
        self.initWithSession();
    });
};

selfactivity.ContentScript.prototype.addContentStylesheet = function() {
    if (!this.addedContentStylesheet) {
        $('html').append( '<link rel="stylesheet" media="screen" href="'+chrome.extension.getURL('css/content-style.css')+'">' );
        this.addedContentStylesheet = true;
    }
}

/**
 * Show semi transparent overlay covering the whole viewport.
 * Don't really know how to do those ui elements in a nice way.
 */
selfactivity.ContentScript.prototype.showOverlay = function() {
    this.addContentStylesheet();
    $('body').append(
        "<div class='selfactivity-overlay'>&nbsp;</div>"
    );
}

selfactivity.ContentScript.prototype.showDialog = function(message, isFinal) {
    var self = this;
    this.addContentStylesheet();
    var dialogHtml = null;
    if (isFinal) {
        dialogHtml = 
            "<div class='selfactivity-dialog'><img src='<%= iconUrl %>'>"+
            "<div class='message'><%= message %></div>"+
            "<div><br/><button class='selfactivity-button selfactivity-report-button' >Show Report</button>"+
            "<button class='selfactivity-button selfactivity-close-button' >Close</button></div>"+
            "</div>";
    } else {
        dialogHtml = 
            "<div class='selfactivity-dialog'><img src='<%= iconUrl %>'>"+
            "<div class='message'><%= message %></div>"+
            "<div><br/><button class='selfactivity-button selfactivity-stop-button' >Stop</button></div>"+
            "</div>";
    }
    var dialogTemplate = _.template(dialogHtml);
    $('body').append(dialogTemplate({
        iconUrl: chrome.extension.getURL('img/icon128.png'),
        message: message
    }));
    $('.selfactivity-stop-button').each(function(){
        if ($(this).hasClass('processed')) {
            return;
        }
        $(this).addClass('processed').click(function(){
            self.clickStopInDialog();
        });
    });
    $('.selfactivity-close-button').each(function(){
        if ($(this).hasClass('processed')) {
            return;
        }
        $(this).addClass('processed').click(function(){
            self.clickCloseInDialog();
        });
    });
    $('.selfactivity-report-button').each(function(){
        if ($(this).hasClass('processed')) {
            return;
        }
        $(this).addClass('processed').click(function(){
            self.clickReportInDialog();
        });
    });
}

selfactivity.ContentScript.prototype.clickStopInDialog = function() {
    if (this.finalActionTimeoutId) {
        clearTimeout(this.finalActionTimeoutId);
    }
    this.session.setEnabled(false);
    this.updateBackgroundSession();
    $(".selfactivity-dialog").remove();
    $(".selfactivity-overlay").remove();
    alert('Automation stopped');
}

selfactivity.ContentScript.prototype.clickCloseInDialog = function() {
    this.session.setEnabled(false);
    this.updateBackgroundSession();
    $(".selfactivity-dialog").remove();
    $(".selfactivity-overlay").remove();
}

selfactivity.ContentScript.prototype.clickReportInDialog = function() {
    this.asyncBackground.openReport();
}

/*
 * ContentScript: *END* 
 *****************************************************************************/

// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License

function parseUri (str) {
    var o   = parseUri.options,
        m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
};

parseUri.options = {
    strictMode: false,
    key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
    q:   {
        name:   "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};
