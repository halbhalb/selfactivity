var selfactivity = selfactivity || {};

selfactivity.Background = function() {
}

selfactivity.Background.prototype.session = null;
selfactivity.Background.prototype.persistence = null;

selfactivity.Background.prototype.init = function() {
    console.log('Init in Background');
	var self = this;
    this.persistence = new selfactivity.Persistence();
    this.resetSession();

    chrome.runtime.onMessage.addListener(
		function(request, sender, sendResponse) {
            console.log('received message');
            console.log(request);
			if (request.method) {
				var method = request.method;
				var params = request["params"];
				var response = self[method](params, sender);
				sendResponse(response);
			}
		});

    chrome.browserAction.onClicked.addListener(
        function(tab) {
            self.resetSession();
            self.session.setSessionTabId( tab.id );
            self.reloadPersistentData();
            chrome.tabs.update(tab.id, {
                'url': chrome.extension.getURL('home.html')
            });
        });
};

selfactivity.Background.prototype.resetSession = function() {
    this.session = new selfactivity.Session();
    this.session.setCurrentSearchResultsPage(1);
    this.session.setRuntimeId(chrome.runtime.id);
}

selfactivity.Background.prototype.getSession = function(ignored, sender) {
    this.session.setCurrentTabId( sender.tab.id );
    return this.session;
};

selfactivity.Background.prototype.setSession = function(sessionData) {
    this.session.loadData(sessionData);
    var persistentData = this.session.getPersistentData();
    this.persistence.storePersistentData(persistentData);
};

selfactivity.Background.prototype.reloadPersistentData = function() {
    this.persistence.loadPersistentData();
    var values = this.persistence.getValues();
    this.session.loadPersistentData(values);
};

selfactivity.Background.prototype.openReport = function() {
    chrome.tabs.create( {
        url: chrome.extension.getURL('report.html')
    });
}

$(function(){
    var background = new selfactivity.Background();
    background.init();
});

