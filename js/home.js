var selfactivity = selfactivity || {};

selfactivity.Home = function() {
}

selfactivity.Home.prototype = new selfactivity.ContentScript();

selfactivity.Home.prototype.initWithSession = function() {
	var errors = this.session.allChecksBeforeStart();
    if (errors) {
        var message = "<div>There are some problems with your settings. You should correct them before you start:</div>";
        for (var key in errors) {
            message += "<div>"+errors[key]+"</div>";
        }
        $(".messages").append('<div class="alert alert-warning">'+message+'</div>');
    } 
    var message = "<div>We will use the following search options:</div>";
    var params = this.session.getSearchParams();
    for (var key in params) {
        if (params[key]) {
            message += "<div>"+key+": "+params[key]+"</div>"
        }
    }
    var maxJobs = this.session.getMaxJobApplications();
    if (!maxJobs) {
        maxJobs = "all";
    }
    message += "<div>Try to apply for maximum "+maxJobs+" jobs</div>";
    var excludedPhrases = this.session.getExcludedPhrases();
    if (excludedPhrases.length) {
        var excludedPhraseList = _.map(excludedPhrases, function(el) {return '<span class="label label-info">'+el+'</span>';}).join(', ');

        message += "<div>Do not apply if job title or description includes "+excludedPhraseList+".</div>";

    }
    $(".messages").append('<div class="alert alert-success">'+message+'</div>');

    var self = this;
    $("a.start-button").click(function(event){
        event.preventDefault();
        self.session.setEnabled(true);
        self.updateBackgroundSession();
        location.href = this.href;
    });
}

$(function(){
    var home = new selfactivity.Home();
    home.init();
});