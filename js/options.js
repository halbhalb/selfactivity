var selfactivity = selfactivity || {};

selfactivity.Options = function() {
}

selfactivity.Options.prototype = new selfactivity.ContentScript();

selfactivity.Options.prototype.LOCATION_AUTOCOMPLETE_URL = "https://jobsearch.direct.gov.uk/Services/AutoComplete.asmx/getTrovixLocationsCompletionList";
selfactivity.Options.prototype.TITLE_AUTOCOMPLETE_URL = "https://jobsearch.direct.gov.uk/Services/AutoComplete.asmx/GetCompletionList";

selfactivity.Options.prototype.persistence = new selfactivity.Persistence();
selfactivity.Options.prototype.asyncBackground = new selfactivity.AsyncBackground();


selfactivity.Options.prototype.collectPersistenOptions = function() {
    var options = {};    
    var optionFun = function(i, el) {
        var sel = $(el);
        var elName= sel.attr('name');
        var elValue= sel.val();
        options[elName] = elValue;
    };
    $(".persistent_option").each(optionFun);
    return options;
}

selfactivity.Options.prototype.init = function() {

    var self = this;

    this.persistence.init();
    this.asyncBackground.init();

    $(".persistent_option").each(function(i, el) {
        var sel = $(el);
        var elName= sel.attr('name');
        $(el).val(self.persistence.getValue(elName));
    });
    
    if( !$("#searchDistance").val() ) {
        $("#searchDistance").val("20");
    }

    if( !$("#actionDelay").val() ) {
        $("#actionDelay").val("5");
    }

    $("#loginFail").val(null);

    $('button.save').click(function() {
        var options = self.collectPersistenOptions();
        _.each(options, function(value, key) {
            self.persistence.setValue(key, value);
        });
        self.asyncBackground.reloadPersistentData();
        alert('Options saved');
        location.reload();
    });

    $("#searchWhere").autocomplete({
        source: function(request, response) {
            var postData = {query:request.term};
            $.ajax({
                type: 'POST',
                url: self.LOCATION_AUTOCOMPLETE_URL,
                contentType: 'application/json',
                data: JSON.stringify(postData),
                dataType:'json',
                success: function(data) {
                    var items = [];
                    var rawItems = data.d.Result.Items;
                    for (var i=0; i<rawItems.length; i++) {
                        items.push($("<span>"+rawItems[i].Text+"</span>").text());
                    }
                    response(items);
                }
            })
        }
    });

    $("#searchTitle").autocomplete({
        source: function(request, response) {
            var postData = { request: { Query:request.term, MaxResults:10, SearchType:'LookupEnhancedSearchJobTitles'}};
            $.ajax({
                type: 'POST',
                url: self.TITLE_AUTOCOMPLETE_URL,
                contentType: 'application/json',
                data: JSON.stringify(postData),
                dataType:'json',
                success: function(data) {
                    var items = [];
                    var rawItems = data.d.Result.Items;
                    for (var i=0; i<rawItems.length; i++) {
                        items.push($("<span>"+rawItems[i].Text+"</span>").text());
                    }
                    response(items);
                }
            })
        }
    });

    $('#excludedPhraseList').tagsinput();

};


$(function(){
    var options = new selfactivity.Options();
    options.init();
})