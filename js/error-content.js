var selfactivity = selfactivity || {};

selfactivity.ErrorContent = function() {
}

selfactivity.ErrorContent.prototype = new selfactivity.ContentScript();

selfactivity.ErrorContent.prototype.initIfCurrent = function() {
    var self = this;
    this.doAfterJobApplication(null, 'error');
};

var contentScript;
$( function(){
    contentScript = new selfactivity.ErrorContent();
    contentScript.init();
});