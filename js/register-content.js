var selfactivity = selfactivity || {};

selfactivity.RegisterContent = function() {
};

selfactivity.RegisterContent.prototype = new selfactivity.ContentScript();

selfactivity.RegisterContent.prototype.loginSelector = "#MasterPage1_HeaderContent_Header_Default_userOptions1_hlLogin";

selfactivity.RegisterContent.prototype.initIfCurrent = function() {
    var self = this;
    $(self.loginSelector).click();
};

var contentScript;
$( function(){
    contentScript = new selfactivity.RegisterContent();
    contentScript.init();
});