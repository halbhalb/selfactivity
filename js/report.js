var selfactivity = selfactivity || {};

selfactivity.Report = function() {
}

selfactivity.Report.prototype = new selfactivity.ContentScript();
selfactivity.Report.prototype.TEMPLATE_JOB = 
    "<tr><td><strong><%= id %></strong></td><td><%= posted %></td><td><%= title %></td><td><%= company %></td><td><%= location %></td></tr><tr><td colspan='5'><a href='https://jobsearch.direct.gov.uk/GetJob.aspx?JobID=<%= id %>' target='_blank'>https://jobsearch.direct.gov.uk/GetJob.aspx?JobID=<%= id %></a></td></tr>";
selfactivity.Report.prototype.TEMPLATE_JOBS = 
    "Applied for the following jobs:<br><table class='table' ><thead><tr><td>ID</td><td>Date posted</td><td>Job Title</td><td>Company</td><td>Location</td></tr></thead><tbody><%= body %></tbody></table>";

selfactivity.Report.prototype.initWithSession = function() {
    var jobsData = this.session.getJobs();
    var count = 0;
    var jobTemplate = _.template(this.TEMPLATE_JOB);
    var jobsTemplate = _.template(this.TEMPLATE_JOBS);
    var jobsBody = '';
    for (var key in jobsData) {
        var jobData = jobsData[key];
        if ('applied'==jobData.status) {
            jobsBody += jobTemplate(jobData);
        }
    }
    if (''!=jobsBody) {
        var completeJobsBody = jobsTemplate({body:jobsBody});
        $('#report-content').html(completeJobsBody);
    } else {
        $('#report-content').html("No jobs applied for");
    }
    
    $(".print-button").click( function() {
        window.print();
    });
};

var contentScript;
$( function(){
    contentScript = new selfactivity.Report();
    contentScript.init();
});