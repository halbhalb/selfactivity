var selfactivity = selfactivity || {};

selfactivity.HomeContent = function() {
}

selfactivity.HomeContent.prototype = new selfactivity.ContentScript();

selfactivity.HomeContent.prototype.initIfCurrent = function() {
    var res = this.doMainSearch();
};

var contentScript;
$( function(){
    contentScript = new selfactivity.HomeContent();
    contentScript.init();
});