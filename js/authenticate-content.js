var selfactivity = selfactivity || {};

selfactivity.AuthenticateContent = function() {
};

selfactivity.AuthenticateContent.prototype = new selfactivity.ContentScript();

selfactivity.AuthenticateContent.prototype.loginSelector = "#ctl00_fixedContent_Username_loginButton";
selfactivity.AuthenticateContent.prototype.usernameSelector = "#ctl00_fixedContent_Username_uid_UIDSingleField";
selfactivity.AuthenticateContent.prototype.passwordSelector = "#ctl00_fixedContent_Username_pw";
selfactivity.AuthenticateContent.prototype.errorSelector = "div.formWarning";

selfactivity.AuthenticateContent.prototype.initIfCurrent = function() {
    var self = this;
    var errorFound = false;
    $(this.errorSelector).each(function(i, element) {
        errorFound = true;
    });
    if (errorFound) {
        this.session.setLoginFail(true);
        this.doFinalActionOnPage(null, "There was a problem while logging in");
        console.log('set login fail to persist');
        console.log(this.session);
    } else {
        this.session.setLoginFail(null);
        this.doFinalActionOnPage(function() {
            $(self.loginSelector).first().each(function() {
                var username = self.session.getUsername();
                var password = self.session.getPassword();
                $(self.usernameSelector).val(username);
                $(self.passwordSelector).val(password);
                $(self.loginSelector).click();
            });
        }, "Logging in");
    }
};

var contentScript;
$( function(){
    contentScript = new selfactivity.AuthenticateContent();
    contentScript.init();
});