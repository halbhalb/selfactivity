var selfactivity = selfactivity || {};

selfactivity.ApplyStartContent = function() {
};

selfactivity.ApplyStartContent.prototype = new selfactivity.ContentScript();

selfactivity.ApplyStartContent.prototype.initIfCurrent = function() {
    var currentJobId = this.session.getCurrentJobId();
    var parsed = parseUri(location.href);
    var urlJobId = parsed.queryKey.jobid;
    if (currentJobId==urlJobId) {
        this.doFinalActionOnPage(function() {
            $('input[type="radio"][name="apply_resume"]').first().prop('checked', true);
            $('input[type="submit"][name="MasterPage1:MainContent:MyPageButtons:btnSubmit"]').first().click();
        }, "Submitting the application");
    } else {
        console.log('the job on page is not the one we should be applying for');
        this.doFinalActionOnPage(null, "Something went wrong!");
    }
};

var contentScript;
$( function(){
    console.log('apply job');
    contentScript = new selfactivity.ApplyStartContent();
    contentScript.init();
});