var selfactivity = selfactivity || {};

selfactivity.ConfirmationContent = function() {
}

selfactivity.ConfirmationContent.prototype = new selfactivity.ContentScript();

selfactivity.ConfirmationContent.prototype.initIfCurrent = function() {
    var self = this;
    var parsed = parseUri(location.href);
    var jobId = parsed.queryKey.aat;
    this.doAfterJobApplication(jobId, 'applied');
};

var contentScript;
$( function(){
    contentScript = new selfactivity.ConfirmationContent();
    contentScript.init();
});