var selfactivity = selfactivity || {};

/**
 * Content script for job search results page.
 * This is the meatiest script as the list of search results is where most of the action
 * happens. 
 * The scripts checks search options and updates them to make sure that all options
 * set by the user are correct.
 * It checks and updates the page number to make sure we go through all search results,
 * not just those immediately shown.
 * It checks that we exhausted the search either by applying for all jobs in the results 
 * or the maximum number of jobs specified by the user.
 */
selfactivity.JobsearchContent = function() {
};

selfactivity.JobsearchContent.prototype = new selfactivity.ContentScript();

selfactivity.JobsearchContent.prototype.SEARCH_RESULTS_SELECTOR = ".JSresults";
selfactivity.JobsearchContent.prototype.DISTANCE_SELECTOR = "#radiusDiv";
selfactivity.JobsearchContent.prototype.POSTED_SELECTOR = "#postingDateDiv";
selfactivity.JobsearchContent.prototype.TYPE_SELECTOR = "#MasterPage1_LeftColumnContent__ctlRefineSearchTrovix_SearchFiltersContainer div";
selfactivity.JobsearchContent.prototype.PAGE_NUMBER_SELECTOR = "#MasterPage1_MainContent__ctlResultsRangeAndPage1_pagesSummary";
selfactivity.JobsearchContent.prototype.PAGE_LINKS_SELECTOR = ".jobPagination .pages";

selfactivity.JobsearchContent.prototype.initIfCurrent = function() {
    //collect some data from the site and session
    this.pageJobs = {};
    this.identifyJobRows();
    this.identifyNumberOfPages();
    //check if we reached the limit of jobs to be applied for set by user
    if (!this.checkNumberOfJobsApplied()) {
        return;
    }
    //check if all the search params on the page are set correctly
    //if they aren't they will be updated and this will be the final action on page
    //so we shouldn't do anything else
    if (!this.checkSearchParams()) {
        return;
    }
    //check if the pageof search results is correct
    //if not, update using final action and do nothing else
    if (!this.checkSearchResultsPage()) {
        return;
    }
    //find a job that has not been touched in this session and try to apply
    //if successful, an application will be started as final action so do nothing else
    if (this.findNewAndApply()) {
        return;
    }   
    //we're on the right page but no job to apply, so end of page reached
    //either we go to the next page or exhausted all pages, check that
    this.processEndOfPage();
};

/**
 * Find the table with job search results on the page and find data of each job displayed:
 * id, link, title, employer.
 * Match the jobs on page to the jobs stored in session and if matches are found
 * show on page the status of those matches.
 */
selfactivity.JobsearchContent.prototype.identifyJobRows = function() {
    var self = this;
    var sessionJobs = this.session.getJobs();
    $(this.SEARCH_RESULTS_SELECTOR).first().each(function() {
        $('tr', this).each(function() {
            self.identifyRow(this);
        })
    });
    var findOne = true;
    for (var id in sessionJobs) {
        var data = sessionJobs[id];
        var pageData = this.pageJobs[id];
        if (pageData && data.status) {
            pageData.status = data.status;
        }
    }
    for (var id in this.pageJobs) {
        var data = this.pageJobs[id];
        var colour = 'green';
        var text = null;
        switch (data.status) {
        case 'applied':
            colour = 'magenta';
            text = 'Applied today';
            break;
        case 'appliedBefore':
            colour = 'orange';
            text = 'Applied before';
            break;
        case 'external':
            colour = 'red';
            text = 'External listing';
            break;
        case 'error':
            colour = 'red';
            text = 'Error';
            break;
        case 'excluded':
            colour = 'red';
            text = 'Excluded by word filter';
            break;
        }
        var row = $('td', data.row).first();
        row.css('background', colour);
        if (text) {
            row.html(text);
        }
    }
};

/**
 * Identify a single job row in the results on page.
 */
selfactivity.JobsearchContent.prototype.identifyRow = function(rowElement) {
    var self = this;
    var jobData = this.findJobDataInRow(rowElement);
    if (jobData) {
        self.session.setJobData(jobData.id, jobData);
        jobData.status = 'new';
        jobData.row = rowElement;
        self.pageJobs[jobData.id] = jobData;
    }
};

/**
 * Find job data; id, title and employer in a row.
 */
selfactivity.JobsearchContent.prototype.findJobDataInRow = function(rowElement) {
    var jobData = null;
    $('a', rowElement ).each(function(){
        var parsed = parseUri(this.href);
        if ('jobsearch.direct.gov.uk'==parsed.host &&
            '/GetJob.ashx' && parsed.path) {
            var jqThis = $(this);
            var parent = jqThis.parent();
            jobData = {
                id: String(parsed.queryKey.JobID).trim(),
                title: String(jqThis.text()).trim(), 
                company: String(parent.next().text()).trim(),
                location: String(parent.next().next().text()).trim(),
                posted: String(parent.prev().prev().text()).trim()
            };
        }
    });
    return jobData;
};

/**
 * Identify the total number of pages and the current page
 * in the current search results.
 */
selfactivity.JobsearchContent.prototype.identifyNumberOfPages = function() {
    this.pageFromSite = 1;
    this.totalPagesFromSite = 1;
    var pageInfoText = $(this.PAGE_NUMBER_SELECTOR).text();
    if (pageInfoText != null) {
        var parts = pageInfoText.split(' ');
        this.pageFromSite = parts[1];
        this.totalPagesFromSite = parts[3];
    }
};

/**
 * Find the first job on page that has not been applied for or identified as not 
 * applicable and try to apply for it.
 */
selfactivity.JobsearchContent.prototype.findNewAndApply = function() {
    var self = this;
    var jobToApply = null;
    var rowToClick = null;
    $(this.SEARCH_RESULTS_SELECTOR+" tr").each(function() {
        var currentJobData = self.findJobDataInRow(this);
        if (jobToApply != null || null==currentJobData) {
            return;
        }  
        var data = self.pageJobs[currentJobData.id];
        if ('new'==data.status) {
            jobToApply = currentJobData;
            rowToClick = this;
            return;
        }
    });
    if (jobToApply) {
        $('td', rowToClick).first().css('background', 'yellow');
        this.session.setCurrentJobId(jobToApply.id);
        this.doFinalActionOnPage(function() {
            $('a', rowToClick).click();
        }, "Applying for job: "+jobToApply.title+", company: "+jobToApply.company);
        return true;
    } 
    return false;
};

/**
 * If all search settings are set properly but we can't find a job to apply,
 * we are at the end of page. We need to react: either advance to the next page
 * or notify user that we're finished if that's the final page.
 */
selfactivity.JobsearchContent.prototype.processEndOfPage = function() {
    if (this.pageFromSite<this.totalPagesFromSite) {
        this.session.setCurrentSearchResultsPage(parseInt(this.pageFromSite)+1);
        this.checkSearchResultsPage();
    } else {
        this.session.setEnabled(false);
        this.doFinalActionOnPage(null, "Processed all jobs in search results.");
    }
};

/**
 * Process what's on the current page and extract all search parameters 
 * used for the current search.
 */
selfactivity.JobsearchContent.prototype.collectPageSearchParams = function() {
    var params = {};    
    var paramsSelector = selfactivity.ContentScript.MAIN_PARAMS_SELECTOR;
    $(paramsSelector+' li a').each(function() {
        var textParts = $(this).text().split(": ");
        if (textParts.length<2) {
            return;
        }
        var paramReadableKey = textParts[0];
        var paramValue = textParts[1].trim();
        switch (paramReadableKey) {
        case 'Skill':
            if (params['Keywords']) {
                params['Keywords'] = params['Keywords']+', '+paramValue;
            } else {
                params['Keywords'] = paramValue;
            }
            break;
        case 'Title':
            if (params['Title']) {
                params['Title'] = params['Title']+', '+paramValue;
            } else {
                params['Title'] = paramValue;
            }
            break;
        case 'Job type':
            params['Type'] = paramValue;
            break;
        case 'City, county, or postcode':
            params['Where'] = paramValue;
            break;
        case 'Distance':
            var valueParts = paramValue.split(' ');
            params['Distance'] = valueParts[0];
            break;
        default:
            params[paramReadableKey] = paramValue;
        }
    });
    return params;
};

/**
 * Compare search parameters in session and those collected from the page.
 * If some are different then try to update search on page to correct that.
 * There are two ways of doing that: 
 * fill all text field in the main search section at once and click search or
 * choose on of the options such in date posted, distance or job type lists and click 
 * on it. The latter can only be done one by one.
 */
selfactivity.JobsearchContent.prototype.checkSearchParams = function() {
    console.log('comparing parameters');
    var self=this;
    this.sessionParams = this.session.getSearchParams();
    console.log('session:');
    console.log(this.sessionParams);
    this.pageParams = this.collectPageSearchParams();
    console.log('page:');
    console.log(this.pageParams);
    this.mismatechedParams = [];
    _.each(this.sessionParams, function(sessionValue, key, list) {
        if (sessionValue== undefined) {
            sessionValue = '';
        }
        sessionValue = sessionValue.toLowerCase();
        var pageValue = self.pageParams[key];
        if (pageValue==undefined) {
            pageValue = '';
        }
        pageValue = pageValue.toLowerCase();
        if (pageValue != sessionValue) {
            self.mismatechedParams.push(key);
        }
    });
    console.log('mismatched:')
    console.log(this.mismatechedParams);
    if (this.mismatechedParams.length == 0) {
        return true;
    }
    var updateFunctionName=null;
    for (var i=0; i<this.mismatechedParams.length; i++ ) {
        var currentParam = this.mismatechedParams[i];
        var currentUpdateFunctionName = "updateSearch"+currentParam;
        if ( typeof this[currentUpdateFunctionName] == 'function' ) {
            updateFunctionName = currentUpdateFunctionName;
            break;
        }
    }
    if (updateFunctionName) {
        this[updateFunctionName]();
    } else {
        this.doMainSearch();
    }
    return false;
};

/**
 * Click on the correct job distance search option to make sure this 
 * setting is like set by user in options.
 */
selfactivity.JobsearchContent.prototype.updateSearchDistance = function() {
    var correctDistance = this.sessionParams.Distance;
    var textToFind = correctDistance + " miles";
    var selector = this.DISTANCE_SELECTOR + " li a";
    var linkToClick = null;
    $(selector).each(function(){
        if ($(this).text() == textToFind) {
            linkToClick = this;
        }
    });
    if (linkToClick) {
        this.doFinalActionOnPage(function() {
            $(linkToClick).click();
        }, "Setting distance to " + textToFind );

    }
};

/**
 * Click on the correct job date posted search option to make sure this 
 * setting is like set by user in options.
 */
selfactivity.JobsearchContent.prototype.updateSearchPosted = function() {
    var correctPosted = this.sessionParams.Posted;
    var textToFind = correctPosted;
    var selector = this.POSTED_SELECTOR + " li a";
    var linkToClick = null;
    $(selector).each(function(){
        if ($(this).text() == textToFind) {
            linkToClick = this;
        }
    });
    if (linkToClick) {
        this.doFinalActionOnPage(function() {
            $(linkToClick).click();
        }, "Setting posting date to: " + textToFind );

    }
};

selfactivity.JobsearchContent.prototype.updateSearchType= function() {
    var correctType = this.sessionParams.Type;
    var textToFind = correctType;
    var selector = this.TYPE_SELECTOR;
    var linkToClick = null;
    $(selector).last().find("li a").each(function(){
        if ($(this).text() == textToFind) {
            linkToClick = this;
        }
    });
    if (linkToClick) {
        this.doFinalActionOnPage(function() {
            $(linkToClick).click();
        }, "Setting job type to: " + textToFind );

    }
};

selfactivity.JobsearchContent.prototype.checkSearchResultsPage = function() {
    var pageFromSession = this.session.getCurrentSearchResultsPage();
    if (this.pageFromSite == pageFromSession) {
        return true;
    }
    if (pageFromSession>this.totalPagesFromSite) {
        this.doFinalActionOnPage(null, 
            "Something went wrong. Asked to go to page "+ pageFromSession +
            " but only "+this.totalPagesFromSite+" pages present");
    }
    //session page doesn't match site page, need to advance
    var linkToClick = null;
    var linkTextToFind = "Page "+pageFromSession;
    //is there direct link to page with that number?
    $(this.PAGE_LINKS_SELECTOR).first().find('a').each(function() {
        if ($(this).text() == linkTextToFind) {
            linkToClick = this;   
        }
    })
    //direct link to page with session number not found, try to find Next/Previous link
    if (linkToClick==null) {
        linkTextToFind = (pageFromSession>this.pageFromSite)?'Next':'Previous';
        $(this.PAGE_LINKS_SELECTOR).first().find('a').each(function() {
            var currentText = $(this).text();
            if (currentText.indexOf(linkTextToFind) !== -1) {
                linkToClick = this;   
            }
        })
    }
    this.doFinalActionOnPage(function() {
        $(linkToClick).click();
    }, "Advancing to page "+pageFromSession+" of search results");
    return false;
};

/**
 * Check if max number of jobs to be applied to has been set by the user and
 * if it has been reached in this session.
 * Retu urn true if we should still be aplying for jobs, false if it's time to stop.
 * If it's time to stop, show a message to user using final action.
 */
selfactivity.JobsearchContent.prototype.checkNumberOfJobsApplied = function() {
    var currentJobs = this.session.getJobApplicationCount();
    var maxJobs = this.session.getMaxJobApplications();
    if (maxJobs && currentJobs>=maxJobs) {
        this.session.setEnabled(false);
        this.doFinalActionOnPage(null, "Reached limit of "+maxJobs+" job applications.");
        return false;
    }
    return true;
}


var contentScript;
$(function(){
    contentScript = new selfactivity.JobsearchContent();
    contentScript.init();
});
