var selfactivity = selfactivity || {};

selfactivity.GetJobContent = function() {
}

selfactivity.GetJobContent.prototype = new selfactivity.ContentScript();

selfactivity.GetJobContent.prototype.initIfCurrent = function() {
    var self = this;
    var currentJobId = this.session.getCurrentJobId();

    var pageHrefParsed = parseUri(location.href);
    var pageJobId = pageHrefParsed.queryKey.JobID;
    if (pageJobId!=currentJobId) {
        console.log('the job on page is not the one we should be applying for');
        this.doFinalActionOnPage(null, "Something went wrong!");
    }

    //save job description to job data
    var currentJobData = this.session.getJobData(currentJobId);
        $('.jobViewContent .jobDescription').first().each(function() {
        currentJobData.text = $(this).text().trim(); 
    });
    this.session.setJobData(currentJobId, currentJobData);

    var found = false;
    var status = null;
    var linkToClick = null;
    $('.jobViewLC .buttonWrapper a').first().each(function(){
        var text=$(this).text();
        if ('Apply'!=text) {
            return;
        }
        if ($(this).attr('href')!='#') {
            var parsed = parseUri(this.href);
            if ('jobsearch.direct.gov.uk'!=parsed.host || 
                '/Apply/ApplyStart.aspx'!=parsed.path) {
                return;
            }
            found=true;
            //we are here because the id of te job matches the session id
            //also the link says Apply and the link is internal within UJ
            //(not pointing to an external listing)
            status='toApply';
            linkToClick = this;
        } else {
            found=true;
            status='appliedBefore';
        }
    });
    if (!found) {
        status='external';
    }
    //if we are going to apply, need to check for filtered words before that
    if (status=='toApply') {
        var titleText = $('.jobViewContent h2').first().next().text();
        if (!titleText) {
            titleText='';
        }
        titleText = titleText.toLowerCase();
        var descriptionText = $('.jobDescription').text();
        if (!descriptionText) {
            descriptionText='';
        }
        descriptionText = descriptionText.toLowerCase();
        var excludedPhrases = this.session.getExcludedPhrases();
        for (var i=0; i<excludedPhrases.length; i++) {
            var phrase = excludedPhrases[i];
            if (titleText.indexOf(phrase)>=0 || descriptionText.indexOf(phrase)>=0) {
                status = 'excluded';    
                break;
            }
        }
    }
    if (status=='toApply') {
        this.doFinalActionOnPage(function() {
            $(linkToClick).click();
        }, 'Following the application process');
    } else {
        self.doAfterJobApplication(pageJobId, status);
    }
    
};

var contentScript;
$( function(){
    contentScript = new selfactivity.GetJobContent();
    contentScript.init();
});